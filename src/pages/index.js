import './index.css'
import 'normalize.css'

import "../utils/importImages"

import Swiper from 'swiper/bundle';
import 'swiper/css/bundle';

const swiper = new Swiper('.swiper2', {
    direction: 'horizontal',
    loop: false,
    spaceBetween: 16,
    slidesPerView: 2,
    width: 270,

});

const swiper1 = new Swiper('.swiper1', {
    direction: 'horizontal',
    loop: false,
    spaceBetween: 16,
    width: 240,

});

const menuBtn = document.getElementById("menu")
const menuBlock = document.querySelector(".menu")
const menuItem1 = document.getElementById("menuFirstItem")
const menuItem2 = document.getElementById("menuSecItem")
const menuItem3 = document.getElementById("menuLastItem")



function open() {
    menuBlock.classList.add('menu__opened')
}

function close() {
    menuBlock.classList.remove('menu__opened')
}

menuBtn.addEventListener('click', () => {
    if (menuBlock.classList.contains('menu__opened')) {
        close(menuBlock)
    } else {
        open(menuBlock)
    }
})

menuItem1.addEventListener('click', () => {
    close(menuBlock)
})
menuItem2.addEventListener('click', () => {
    close(menuBlock)
})
menuItem3.addEventListener('click', () => {
    close(menuBlock)
})

function zeroFirstFormat(value) {
    if (value < 10) {
        value = '0' + value;
    }
    return value;
}


function date() {
    const current_datetime = new Date();
    const day = zeroFirstFormat(current_datetime.getDate());
    const month = zeroFirstFormat(current_datetime.getMonth() + 1);
    const year = current_datetime.getFullYear();

    return day + "." + month + "." + year
}

document.querySelector(".news__card_info_date").innerHTML = date();

